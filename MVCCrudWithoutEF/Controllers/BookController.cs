using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCrudWithoutEF.Models;
using Microsoft.Extensions.Configuration;
using MVCCrudWithoutEF.Interfaces;

namespace MVCCrudWithoutEF.Controllers
{
    public class BookController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IBooks _ibooks;

        public BookController(IConfiguration configuration, IBooks bookList)
        {
            this._configuration = configuration;
            this._ibooks = bookList;
        }

        // GET: Book
        public ActionResult Index()
        {
            var BookList = _ibooks.BookList();
            
            return View(BookList);
        }

        // GET: Book/Add/
        public ActionResult Add(int id)
        {
            BookViewModel bookViewModel = new BookViewModel();
            return View(bookViewModel);
        }

        // POST: Book/Add/
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                var AddBook = _ibooks.AddBook(book);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Book/Edit/5
        public ActionResult Edit(int id)
        {

            //ViewBag.Data = Book;

            ViewBag.Data = _ibooks.GetEditBook(id);
            return View(_ibooks.GetEditBook(id));
        }

        // POST: Book/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, BookViewModel book)
        {
            if (ModelState.IsValid)
            {
                var editBook = _ibooks.EditBook(id, book);
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        // GET: Book/Delete/5
        [HttpGet]
        public ActionResult Delete(int id)
        {
            ViewBag.Data = _ibooks.GetDeleteBook(id);
            return View(_ibooks.GetDeleteBook(id));
        } 

        // POST: Book/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteBook(int id)
        {
            var deleteBook = _ibooks.DeleteBook(id);
            Console.WriteLine(deleteBook);
            return RedirectToAction(nameof(Index));
        }
    }
}