using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MVCCrudWithoutEF.Interfaces;
using MVCCrudWithoutEF.Models;

namespace MVCCrudWithoutEF.Controllers
{
    public class BookApiController : Controller
    {
        private readonly IBooks _ibooks;

        public BookApiController(IBooks bookList)
        {
            this._ibooks = bookList;
        }

        [HttpGet, ActionName("GetBook")]
        public IActionResult GetBook()
        {
            var result = _ibooks.BookList();
            return Json(new { status = 200, data = result });
        }

        [HttpPost, ActionName("SubmitBook")]
        public IActionResult SubmitBook([FromBody] BookViewModel requestBody)
        {
            var addBook = _ibooks.AddBook(requestBody);

            if(addBook)
            {
                return Json(new { status = 200, message = "Success" });
            }
            else
            {
                return Json(new { status = 500, message = "Failed" });
            }
        }

        [HttpGet, ActionName("GetEditBook")]
        public IActionResult GetEditBook(int id)
        {
            var result = _ibooks.GetEditBook(id);

                return Json(new { status = 200, data = result }); 
        }

        [HttpPut, ActionName("UpdateBook")]
        public IActionResult UpdateBook(int id,[FromBody] BookViewModel requestBody)
        {
            var updateBook = _ibooks.EditBook(id, requestBody);

            if(updateBook)
            {
                return Json(new { status = 200, message = "Succesfully updated" });
            }
            else
            {
                return Json(new { status = 500, message = "Error" });
            }
        }

        [HttpDelete, ActionName("DeleteBook")]
        public IActionResult DeleteBook(int id)
        {
            var deleteBook = _ibooks.DeleteBook(id);

            if(deleteBook)
            {
                return Json(new { status = 200, message = "Book Deleted" });
            }
            else
            {
                return Json(new { status = 500, message = "Error" });
            }
        }
    }
}