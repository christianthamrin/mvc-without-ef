﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCCrudWithoutEF.Models;
using Microsoft.Extensions.Configuration;
using MVCCrudWithoutEF.Interfaces;

namespace MVCCrudWithoutEF.Services
{
    public class BookServices : IBooks
    {
        private IConfiguration _configuration;

        public BookServices(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        //GET: Book List in Index
        public List<BookViewModel> BookList()
        {
            List<BookViewModel> bookList = new List<BookViewModel>();
            try
            {
                using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
                {
                    string sql = "SELECT * FROM Books";
                    SqlCommand cmd = new SqlCommand(sql, sqlConnection);
                    sqlConnection.Open();

                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            var book = new BookViewModel();
                            book.BookID = Convert.ToInt32(reader["BookID"].ToString());
                            book.Title = reader["Title"].ToString();
                            book.Author = reader["Author"].ToString();
                            book.Price = Convert.ToInt32(reader["Price"].ToString());
                            bookList.Add(book);

                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e);
            }

            return (bookList);
        }

        //POST: Submit Add Book
        public bool AddBook(BookViewModel book)
        {
            var result = false;
            using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
                
            {
               string sql = "INSERT into Books (Title, Author, Price) VALUES (@Title, @Author, @Price)";

                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@Title", book.Title);
                command.Parameters.AddWithValue("@Author", book.Author);
                command.Parameters.AddWithValue("@Price", book.Price);

                try
                {
                    sqlConnection.Open();
                    int recordsAffected = command.ExecuteNonQuery();
                    result = true;
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    sqlConnection.Close();
                }

            }
            return result;
            
        }

        //GET: Edit Book Form
        public BookViewModel GetEditBook(int? id)
        {
            DataTable dtable = new DataTable();
            BookViewModel Book = new BookViewModel();


            using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
            {
                sqlConnection.Open();
                string sql = "SELECT * FROM Books WHERE BookID = @BookID";
                SqlDataAdapter sqlDa = new SqlDataAdapter(sql, sqlConnection);
                sqlDa.SelectCommand.Parameters.AddWithValue("@BookID", id);
                sqlDa.Fill(dtable);
            }
            try
            {
                if (dtable.Rows.Count == 1)
                {
                    Book.BookID = Convert.ToInt32(dtable.Rows[0][0].ToString());
                    Book.Title = dtable.Rows[0][1].ToString();
                    Book.Author = dtable.Rows[0][2].ToString();
                    Book.Price = Convert.ToInt32(dtable.Rows[0][3].ToString());
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return Book;
        }

        //POST: Update Book
        public bool EditBook(int id, BookViewModel book)
        {
            var result = false;

            using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
            {
                string sql = "UPDATE Books SET Title = @Title, Author = @Author, Price = @Price WHERE BookID = @BookID";

                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@BookID", id);
                command.Parameters.AddWithValue("@Title", book.Title);
                command.Parameters.AddWithValue("@Author", book.Author);
                command.Parameters.AddWithValue("@Price", book.Price);

                try
                {
                    sqlConnection.Open();
                    int recordsAffected = command.ExecuteNonQuery();
                    result = true;
                }
                catch (SqlException e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    sqlConnection.Close();
                }

            }
            return result;
        }

        public BookViewModel GetDeleteBook(int id)
        {
            DataTable dtable = new DataTable();
            BookViewModel book = new BookViewModel();

            using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
            {
                string sql = "SELECT * FROM Books WHERE BookID = @BookID";

                SqlDataAdapter sqlDa = new SqlDataAdapter(sql, sqlConnection);
                sqlDa.SelectCommand.Parameters.AddWithValue("@BookID", id);
                sqlDa.Fill(dtable);

                try
                {
                    sqlConnection.Open();

                    if(dtable.Rows.Count == 1)
                    {
                        book.BookID = Convert.ToInt32(dtable.Rows[0][0].ToString());
                        book.Title = dtable.Rows[0][1].ToString();
                        book.Author = dtable.Rows[0][2].ToString();
                        book.Price = Convert.ToInt32(dtable.Rows[0][3].ToString());
                    }
                    else
                    {
                        return RedirectToAction("Index");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                return book;
            }

        }

        public bool DeleteBook(int id)
        {
            var result = false;

            using (SqlConnection sqlConnection = new SqlConnection(_configuration.GetConnectionString("Default")))
            {
                string sql = "DELETE FROM Books WHERE BookID = @BookID";

                SqlCommand command = new SqlCommand(sql, sqlConnection);
                command.Parameters.AddWithValue("@BookID", id);

                try
                {
                    sqlConnection.Open();
                    int recordsAffected = command.ExecuteNonQuery();
                    result = true;
                    Console.WriteLine(id);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                finally
                {
                    sqlConnection.Close();
                }
            }

            return result;
        }

        private BookViewModel RedirectToAction(string v)
        {
            throw new NotImplementedException();
        }
    }
}
        
