﻿using System;
using System.Collections.Generic;
using MVCCrudWithoutEF.Models;

namespace MVCCrudWithoutEF.Interfaces
{
    public interface IBooks
    {
        public List<BookViewModel> BookList();
        public bool AddBook(BookViewModel book);
        public BookViewModel GetEditBook(int? id);
        public bool EditBook(int id, BookViewModel book);
        public BookViewModel GetDeleteBook(int id);
        public bool DeleteBook(int id);
    }
}
